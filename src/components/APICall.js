import { Fragment, useState, useEffect, React } from "react";

export const APICall = () => {
  const [isClicked, setIsClicked] = useState(false);
  const handleClick = () => {
    setIsClicked(!isClicked);
  };

  const [resourceType, setResourceType] = useState("");
  const [items, setItems] = useState([]);
  const resetClick = () => setResourceType("");

  useEffect(() => {
    fetch(`https://jsonplaceholder.typicode.com/${resourceType}`)
      .then((response) => response.json()) // returning a promise that resolves w/ json data from response body
      .then((json) => setItems(json))
      .catch((e) => console.log("error! see further:", e));
  }, [resourceType]); //this side effect will only run when resourceType is changed!

  return (
    <Fragment>
      <div id="wrapper" class="bg-purple-300">
        <div id="accordian-item-wrapper" class="border-black border-2">
          <div
            id="accordian-data-wrapper"
            class={`${isClicked ? "bg-blue-200 h-fit pb-2" : ""}`}
            onClick={handleClick}
          >
            <div id="title" class="bg-blue-100 text-center">
              3RD PARTY API CALL
            </div>
            {isClicked && (
              <div
                class="accordian-content"
                onClick={(e) => e.stopPropagation()}
              >
                <div className="text-center border-2 border-purple-800 bg-purple-50 m-4 p-4 shadow-lg shadow-purple-200 ">
                  <h1 className="mb-5 text-2xl">
                    {" "}
                    Toggle What'd You'd Like to See{" "}
                  </h1>
                  <button
                    className="border-2 border-purple-800 mr-3 p-2 bg-white"
                    onClick={() => setResourceType("posts")}
                  >
                    Posts
                  </button>
                  <button
                    className="border-2 border-purple-800 mr-3 p-2 bg-white"
                    onClick={() => setResourceType("users")}
                  >
                    Users
                  </button>
                  <button
                    className="border-2 border-purple-800 mr-3 p-2 bg-white "
                    onClick={() => setResourceType("comments")}
                  >
                    Comments
                  </button>
                  <button
                    className="border-2 border-purple-800 mr-3 p-2 bg-white"
                    onClick={() => setResourceType("photos")}
                  >
                    Photos
                  </button>

                  <button
                    onClick={resetClick}
                    className="border-2 border-purple-800 mr-3 p-2 bg-white"
                  >
                    {" "}
                    Reset{" "}
                  </button>

                  {resourceType !== "" ? (
                    <div className="border-2 border-purple-400 bg-blue-50 mt-10 p-2 w-full">
                      <h1 className="font-mono font-bold text-lg uppercase mt-1 mb-3">
                        {resourceType}
                      </h1>
                      {items.slice(0, 5).map((item) => {
                        if (resourceType === "comments") {
                          return (
                            <>
                              <span>
                                → {JSON.stringify(item["name"])}
                                <br />
                              </span>
                            </>
                          );
                        } else if (resourceType === "posts") {
                          return (
                            <>
                              <span>
                                → {JSON.stringify(item["title"])}
                                <br />
                              </span>
                            </>
                          );
                        } else if (resourceType === "users") {
                          return (
                            <>
                              <span>
                                → {JSON.stringify(item["name"])}
                                <br />
                              </span>
                            </>
                          );
                        } else if (resourceType === "photos") {
                          return (
                            <div className="flex flex-wrap flex-row">
                              <img
                                key={item.id}
                                className="mr-2 p-4"
                                src={item["thumbnailUrl"]}
                                alt={`Thumbnail for Photo ${item.id}`}
                              />
                            </div>
                          );
                        }
                      })}
                    </div>
                  ) : null}
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </Fragment>
  );
};
