import { useEffect, useState, Fragment } from "react";

export function Timer() {
  const handleClick = () => {
    setIsClicked(!isClicked);
  };

  const [isClicked, setIsClicked] = useState(false);

  const calculateTimeLeft = () => {
    let year = new Date().getFullYear();
    const difference = +new Date(`${year}-08-28`) - +new Date();
    let timeLeft = {};

    if (difference > 0) {
      timeLeft = {
        days: Math.floor(difference / (1000 * 60 * 60 * 24)),
        hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
        minutes: Math.floor((difference / 1000 / 60) % 60),
        seconds: Math.floor((difference / 1000) % 60),
      };
    }

    return timeLeft;
  };

  const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());
  const [year] = useState(new Date().getFullYear());

  useEffect(() => {
    setTimeout(() => {
      setTimeLeft(calculateTimeLeft());
    }, 1000);
  });

  const timerComponents = [];

  Object.keys(timeLeft).forEach((interval) => {
    if (!timeLeft[interval]) {
      return;
    }

    timerComponents.push(
      <span class="text-center">
        {timeLeft[interval]} {interval}{" "}
      </span>
    );
  });

  return (
    <Fragment>
      <div id="wrapper" class="bg-purple-300">
        <div id="accordian-item-wrapper" class="border-black border-2">
          <div
            id="accordian-data-wrapper"
            class={`${isClicked ? "bg-blue-200 h-fit pb-2" : ""}`}
            onClick={handleClick}
          >
            <div id="title" class="bg-blue-100 text-center ">
              COUNT DOWN
            </div>
            {isClicked && (
              <div
                class="accordian-content"
                onClick={(e) => e.stopPropagation()}
              >
                <div className="border-2 border-orange-400 bg-yellow-50 m-4 p-4">
                  <h1 className="text-center text-2xl">Technical Interview </h1>
                  <h2 className="mb-5 text-center"> good luck! </h2>
                  <span className="font-mono bg-black text-green-400 p-2 border-2 border-lime-600 outline-2 outline-offset-4 outline-dashed justify-center grid">
                    {timerComponents.length ? (
                      timerComponents
                    ) : (
                      <span>Time's up!</span>
                    )}
                  </span>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </Fragment>
  );
}
