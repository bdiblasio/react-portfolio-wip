import { React } from "react";
import { Search } from "./Search";
import { Counter } from "./Counter";
import { Timer } from "./Timer";
import { WindowSize } from "./WindowSize";
import { Checkbox } from "./Checkbox";
import { ChatForm } from "./ChatForm";
import { APICall } from "./APICall";
import { ClickChoice } from "./ClickChoice";


export const Accordian = () => {
  return (
    <>
      <div id="components-wrapper" class="m-3">
        <div id="first-component" class="mb-4">
          <Counter />
        </div>
        <div id="first-component" class="mb-4">
          <Search />
        </div>
        <div id="first-component" class="mb-4">
          <Timer />
        </div>
        <div id="first-component" class="mb-4">
          <WindowSize />
        </div>
        <div id="first-component" class="mb-4">
          <Checkbox/>
        </div>
        <div id="first-component" class="mb-4">
          <ChatForm/>
        </div>
        <div id="first-component" class="mb-4">
          <APICall/>
        </div>
        <div id="first-component" class="mb-4">
          <ClickChoice/>
        </div>
      </div>
    </>
  );
};
