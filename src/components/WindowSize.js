import { Fragment, useState, useEffect, React } from "react";

export const WindowSize = () => {
  const [isClicked, setIsClicked] = useState(false);
  const handleClick = () => {
    setIsClicked(!isClicked);
  };

  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  const [windowHeight, setWindowHeight] = useState(window.innerHeight);

  const handleResize = () => {
    setWindowWidth(window.innerWidth);
    setWindowHeight(window.innerHeight);
  };

  useEffect(() => {
    window.addEventListener("resize", handleResize);
  }, []);

  return (
    <Fragment>
      <div id="wrapper" class="bg-purple-300">
        <div id="accordian-item-wrapper" class="border-black border-2">
          <div
            id="accordian-data-wrapper"
            class={`${isClicked ? "bg-blue-200 h-fit pb-2" : ""}`}
            onClick={handleClick}
          >
            <div id="title" class="bg-blue-100 text-center ">
              WINDOW SIZE TRACKER
            </div>
            {isClicked && (
              <div
                class="accordian-content"
                onClick={(e) => e.stopPropagation()}
              >
                <div className="border-2 border-blue-500 bg-blue-50 m-4 p-4">
                  <h1 className="text-center text-2xl mb-5">
                    {" "}
                    Adjust Site to See Size
                  </h1>
                  <div className="text-center">
                    {" "}
                    The Window Width is: {windowWidth}px
                  </div>
                  <div class="text-center">
                    {" "}
                    The Window Height is: {windowHeight}px
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </Fragment>
  );
};
