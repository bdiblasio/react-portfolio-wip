import { Accordian } from "./Accordion";
import { AboutMe } from "./AboutMe";
import { Footer } from "./Footer";

export const Homepage = () => {
  return (
    <>
      <AboutMe />
      <Accordian />
      <Footer />
    </>
  );
};
