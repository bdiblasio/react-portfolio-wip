import { Fragment, useState, React } from "react";

export const ChatForm = () => {
  const [isClicked, setIsClicked] = useState(false);
  const handleClick = () => {
    setIsClicked(!isClicked);
  };

  let [theirName, setTheirName] = useState("");
  let [email, setEmail] = useState("");
  let [phone, setPhone] = useState("");
  let [message, setMessage] = useState("");
  let [submitted, setSubmitted] = useState(false);
  let [submittedName, setSubmittedName] = useState("");
  let [submittedMessage, setSubmittedMessage] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    setSubmitted(true);
    setSubmittedName(theirName);
    setSubmittedMessage(message);
    setTheirName("");
    setEmail("");
    setPhone("");
    setMessage("");
  };

  const handleReset = (e) => {
    setTheirName("");
    setEmail("");
    setPhone("");
    setMessage("");
    setSubmitted(false);
    setSubmittedName("");
    setSubmittedMessage("");
  };

  return (
    <Fragment>
      <div id="wrapper" class="bg-purple-300">
        <div id="accordian-item-wrapper" class="border-black border-2">
          <div
            id="accordian-data-wrapper"
            class={`${isClicked ? "bg-blue-200 h-fit pb-2" : ""}`}
            onClick={handleClick}
          >
            <div id="title" class="bg-blue-100 text-center ">
              SIMPLE FORM
            </div>
            {isClicked && (
              <div
                class="accordian-content"
                onClick={(e) => e.stopPropagation()}
              >
                <div className="chat-form m-4 p-2 border-2 border-green-800 bg-green-100 text-center">
                  <div className="greeting mb-4 ml-3 m-5">
                    {/* //IF THE FORM HAS BEEN SUBMITTED, THIS WILL RENDER */}
                    {submitted ? (
                      <>
                        <p className="greeting-submitted text-2xl mt-5 mb-5">
                          Hello, {submittedName}.
                        </p>
                        <p>
                          {" "}
                          Thank you for your message: <br />
                        </p>
                        <p className="w-full mt-5 bg-white w-96 h-fit p-4 shadow-lg shadow-lime-700 mb-5 rounded-md lowercase">
                          {" "}
                          {'"'}
                          {submittedMessage}
                          {'"'}
                        </p>
                        <p> Want to send another? </p>
                        <button
                          type="reset"
                          onClick={handleReset}
                          className="bg-white w-60 border-green-400 border-2 p-2 mt-5"
                        >
                          {" "}
                          Reset{" "}
                        </button>
                      </>
                    ) : (
                      <>
                        {/* OTHERWISE THIS WILL RENDER */}
                        <p className="greeting-submitted  text-2xl mb-5">
                          Please Submit the Form
                        </p>
                        <form onSubmit={handleSubmit} className="ml-3">
                          <input
                            className="border-2 border-black p-1 w-96 mb-2"
                            name="theirName"
                            value={theirName}
                            onChange={(e) => setTheirName(e.target.value)}
                            placeholder="NAME"
                          />
                          <br />
                          <input
                            className="border-2 border-black p-1 w-96 mb-2"
                            name="email"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            placeholder="EMAIL"
                          />
                          <br />
                          <input
                            className="border-2 border-black p-1 w-96 mb-2"
                            name="phone"
                            value={phone}
                            onChange={(e) => setPhone(e.target.value)}
                            placeholder="PHONE"
                          />
                          <br />
                          <input
                            className="border-2 border-black p-1 w-96 mb-2"
                            name="message"
                            value={message}
                            onChange={(e) => setMessage(e.target.value)}
                            placeholder="MESSAGE"
                          />
                          <br />
                          <button
                            type="submit"
                            className="bg-white border-green-400 w-60 border-2 mt-10 p-2"
                          >
                            {" "}
                            Submit{" "}
                          </button>
                          <br />
                          <button
                            type="reset"
                            onClick={handleReset}
                            className="bg-white w-60 border-green-400 border-2 mt-2 p-2 mb-10"
                          >
                            {" "}
                            Reset{" "}
                          </button>
                        </form>
                      </>
                    )}
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </Fragment>
  );
};
