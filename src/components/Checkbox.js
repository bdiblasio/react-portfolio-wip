import { Fragment, useState, useEffect, React } from "react";
import { TarotCards } from "../data/TarotCards";

export const Checkbox = () => {
  const [isClicked, setIsClicked] = useState(false);
  const handleClick = () => {
    setIsClicked(!isClicked);
  };
  const [isMajorArcana, setIsMajorArcana] = useState(false);
  const [isMinorArcana, setIsMinorArcana] = useState(false);
  const [isCups, setIsCups] = useState(false);
  const [isSwords, setIsSwords] = useState(false);
  const [isWands, setIsWands] = useState(false);
  const [isPentacles, setIsPentacles] = useState(false);

  const filteredData = TarotCards.filter((card) => {
    if (
      (isMajorArcana && card.arcana === "Major") ||
      (isMinorArcana && card.arcana === "Minor") ||
      (isCups && card.suit === "Cups") ||
      (isSwords && card.suit === "Swords") ||
      (isWands && card.suit === "Wands") ||
      (isPentacles && card.suit === "Pentacles")
    ) {
      return true;
    }
    return false;
  });

  return (
    <Fragment>
      <div id="wrapper" class="bg-purple-300">
        <div id="accordian-item-wrapper" class="border-black border-2">
          <div
            id="accordian-data-wrapper"
            class={`${isClicked ? "bg-blue-200 h-fit pb-2" : ""}`}
            onClick={handleClick}
          >
            <div id="title" class="bg-blue-100 text-center ">
              SEARCH BY CHECKBOX
            </div>
            {isClicked && (
              <div
                class="accordian-content"
                onClick={(e) => e.stopPropagation()}
              >
                <div className="border-2 border-green-800 bg-green-50 m-4 p-4 shadow-lg shadow-lime-200 ">
        <h1 className="text-center text-2xl mb-5"> What Would You Like To See? </h1>
<div id="checkboxes" class="text-center ">
      <label className="pr-5">
        <input
          type="checkbox"
          className="mr-2"
          checked={isMajorArcana}
          onChange={() => setIsMajorArcana(!isMajorArcana)}
        />
        Major Arcana
      </label>

      <label className="pr-5">
        <input
          type="checkbox"
          className="mr-2"
          checked={isMinorArcana}
          onChange={() => setIsMinorArcana(!isMinorArcana)}
        />
        Minor Arcana
      </label>

      <label className="pr-5">
        <input
          type="checkbox"
          className="mr-2"
          checked={isCups}
          onChange={() => setIsCups(!isCups)}
        />
        Cups
      </label>

      <label className="pr-5">
        <input
          type="checkbox"
          className="mr-2"
          checked={isWands}
          onChange={() => setIsWands(!isWands)}
        />
        Wands
      </label>

      <label className="pr-5">
        <input
          type="checkbox"
          className="mr-2"
          checked={isPentacles}
          onChange={() => setIsPentacles(!isPentacles)}
        />
        Pentacles
      </label>

      <label>
        <input
          type="checkbox"
          className="mr-2"
          checked={isSwords}
          onChange={() => setIsSwords(!isSwords)}
        />
        Swords
      </label>
</div>

{ isMajorArcana || isMinorArcana || isCups || isWands || isPentacles || isSwords ? (
      <table className="border-2 w-full text-center mt-5">
        <thead className="mt-1 p-4">
          <tr className="p-3 mt-5 bg-blue-400">
            <th>Name</th>
            <th>Arcana</th>
            <th>Suit</th>
          </tr>
        </thead>
        <tbody className="">
          {filteredData.map((card, index) => (
            <tr key={index} className="border-2 border-blue-500">
              <td className="pl-4 w-48">{card.name}</td>
              <td className="w-48">{card.arcana}</td>
              <td className="w-48">{card.suit}</td>
            </tr>
          ))}
        </tbody>
      </table>
) : ( "" )
          }
    </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </Fragment>
  );
};
