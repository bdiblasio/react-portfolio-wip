import React from "react"

export const AboutMe = () => {
    return (
        <div id="page-wrapper" class="bg-purple-100 text-4xl text-center h-36">
            BERNADETTE DI BLASIO
        </div>
    )
}
