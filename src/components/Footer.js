import React from "react";

export const Footer = () => {
  return (
    <div
      id="footer-container"
      class=" bg-blue-200 fixed
    inset-x-0
    bottom-0"
    >
      <div
        id="marquee-container"
        class="bg-purple-100 p-2 relative flex overflow-x-hidden"
      >
        <div id="marquee-content-1" class="animate-marquee whitespace-nowrap ">
          <span class="mx-4 text-2xl"> React </span>
          <span class="mx-4 text-2xl"> Tailwind </span>
          <span class="mx-4 text-2xl"> Javascript </span>
          <span class="mx-4 text-2xl"> HTML </span>
          <span class="mx-4 text-2xl"> Python </span>
          <span class="mx-4 text-2xl"> Docker </span>
          <span class="mx-4 text-2xl"> Django </span>
          <span class="mx-4 text-2xl"> Laravel </span>
          <span class="mx-4 text-2xl"> MaterialUI </span>
          <span class="mx-4 text-2xl"> PostgreSQL </span>
          <span class="mx-4 text-2xl"> PHP </span>
        </div>
        <div
          id="marquee-content-2"
          class="p-2 absolute top-0 animate-marquee2 whitespace-nowrap"
        >
          <span class="mx-4 text-2xl"> React </span>
          <span class="mx-4 text-2xl"> Tailwind </span>
          <span class="mx-4 text-2xl"> Javascript </span>
          <span class="mx-4 text-2xl"> HTML </span>
          <span class="mx-4 text-2xl"> Python </span>
          <span class="mx-4 text-2xl"> Docker </span>
          <span class="mx-4 text-2xl"> Django </span>
          <span class="mx-4 text-2xl"> Laravel </span>
          <span class="mx-4 text-2xl"> MaterialUI </span>
          <span class="mx-4 text-2xl"> PostgreSQL </span>
          <span class="mx-4 text-2xl"> PHP </span>
        </div>
      </div>
    </div>
  );
};
