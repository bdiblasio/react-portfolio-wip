import { Fragment, useState, React, useEffect} from "react";
import { TarotCards } from "../data/TarotCards";

export const Search = () => {
  const [isClicked, setIsClicked] = useState(false)
  const [searchTerm, setSearchTerm] = useState("");
  const [filteredData, setFilteredData] = useState(TarotCards);

  const handleClick = () => {
    setIsClicked(!isClicked)
  }

  const handleSearchChange = (e) => {
    const searchValue = e.target.value;
    setSearchTerm(searchValue);

    const filtered = TarotCards.filter((item) =>
      Object.values(item).some((fieldValue) =>
        String(fieldValue).toLowerCase().includes(searchValue.toLowerCase())
      )
    );
    setFilteredData(filtered);
  };

  return (
    <Fragment>
      <div id="wrapper" class="bg-purple-300">
        <div id="accordian-item-wrapper" class="border-black border-2">
          <div id="accordian-data-wrapper"
            class={`${isClicked ? 'bg-blue-200 h-fit pb-2': ''}`}
            onClick={handleClick}>
            <div id="title" class="bg-blue-100 text-center ">SEARCH</div>
          {isClicked &&
          (<div class="accordian-content" onClick={(e) => e.stopPropagation()}>
            <div className="border-2 border-green-800 bg-green-50 m-4 p-4 shadow-lg shadow-lime-200 justify-center ">
        <h1 className="font-rock text-2xl text-center">
            Please Search for a Tarot Card
        </h1>
        <input
          type="text"
          placeholder="by name, arcana, or suit"
          value={searchTerm}
          onChange={handleSearchChange}
          className="bg-lime-100 p-2 border-2 mt-5 w-full mb-5 text-center"
        />
        {searchTerm ? (
          <table className="border-2 w-3/6 w-full">
            <thead className="mt-1 p-4 text-center">
              <tr className="p-3 mt-5 bg-blue-400 text-center">
                <th>Name</th>
                <th>Arcana</th>
                <th>Suit</th>
              </tr>
            </thead>
            <tbody className="">
              {filteredData.map((item, index) => (
                <tr key={index} className="border-2 border-blue-500">
                  <td className="pl-4 w-48 text-center">{item.name}</td>
                  <td className="w-48 text-center">{item.arcana}</td>
                  <td className="w-48 text-center">{item.suit}</td>
                </tr>
              ))}
            </tbody>
          </table>
        ) : (
          ""
        )}

      </div>


             </div>)}
        </div>
        </div>
      </div>
    </Fragment>
  );
};
