import { Fragment, useState, React } from "react";

export const Counter = () => {
  const [isClicked, setIsClicked] = useState(false);
  let [count, setCount] = useState(0);
  const increase = () => setCount(count + 1);
  const decrease = () => setCount(count - 1);

  const handleClick = () => {
    setIsClicked(!isClicked);
  };

  return (
    <Fragment>
      <div id="wrapper" class="bg-purple-300">
        <div id="accordian-item-wrapper" class="border-black border-2">
          <div
            id="accordian-data-wrapper"
            class={`${isClicked ? "bg-blue-200 h-fit pb-2" : ""}`}
            onClick={handleClick}
          >
            <div id="title" class="bg-blue-100 text-center ">
              COUNTER
            </div>
            {isClicked && (
              <div
                class="accordian-content"
                onClick={(e) => e.stopPropagation()}
              >
                <div class="border-2 border-green-400 bg-green-50 m-4 p-4 ">
                  <div class="text-2xl text-center"> {count}</div>
                  <div class="buttons justify-center grid ">
                    <button
                      onClick={increase}
                      class="border-2 border-green-400 p-2 mt-4 w-96"
                    >
                      {" "}
                      Go Up ▲{" "}
                    </button>
                    <button
                      onClick={decrease}
                      class="border-2 border-green-400 p-2 mt-4 w-96"
                    >
                      {" "}
                      Go Down ▼{" "}
                    </button>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </Fragment>
  );
};

// First, we need to import the useState function, and React from the react library.
// I'm choosing useState, because it's a sleek way to manage state in this component
// We need to define our functional component, I'll call it Counter
// Now, lets add our first piece of state, called count.
// Count needs a counterpart, named setCount, which is a function that handles any updates to 'count'
// We pass the useState function the value 0, to initialize the 'count' variable (this could be any number)
// Next, we define two more functions, increase and decrease
// These functions will manipulate the data in setCount to be whatever the sum of (count +/- 1) is
// Skipping down to the buttons, because the rest is pretty basic HTML with tailwind...
// Each button has an onClick attribute that will specify what function to call when its clicked.
